import java.util.Scanner;

import multUtils.IMatrixFactory;
import multUtils.MatrixArrayRandom;
import multUtils.MatrixFactory;
import multUtils.MatrixMultCost;
import multUtils.MultAsocMatrices;

public class main {
	public static void main(final String[] args) {
		Scanner scanner = new Scanner( System.in );
		
		int numMatrices;
		int numRows, numCols;
		char opcion;
		boolean salir;
		
		MultAsocMatrices mam;
		IMatrixFactory matrixFactory = new MatrixFactory();
		
		do {
			mam = new MultAsocMatrices( matrixFactory );
			
			System.out.println("Cuantas matrices quiere multiplicar?");
			numMatrices = scanner.nextInt();
			
			for( int i = 0; i < numMatrices; i++ ) {
				do {
					salir = true;
					System.out.printf("Matriz %d:\n", i);
					
					System.out.println ("  Cuantas filas tiene?");
					numRows = scanner.nextInt();
					
					System.out.println("  Cuantas columnas tiene?");
					numCols = scanner.nextInt();
					
					try {
						mam.addMatrix( new MatrixArrayRandom(numRows, numCols) );
					} catch (Exception e) {
						// TODO: handle exception
						salir = false;
						System.out.println("[ERROR] La matriz introducida no es valida, introduzcala de nuevo.");
					}
				} while (!salir);
			}
		
			MatrixMultCost mmc = mam.getMultOrder();
		
			System.out.printf("El orden con coste minimo es ' %s ' y tiene un coste de %d operaciones.\n\n", mmc.getMatrix().toString(), mmc.getCost() );
			
			System.out.println("La matriz resultante es:");
			
			int width = mmc.getMatrix().getWidth();
			int height = mmc.getMatrix().getHeight();
			double[][] resultado = mmc.getMatrix().getValue();
			
			for( int i = 0; i<height; i++ ) {
				System.out.printf("  [ %.3f", resultado[i][0] );
				for( int j = 1; j<width; j++ ) {
					System.out.printf(", %.3f", resultado[i][j] );
				}
				System.out.println(" ]");	
			}						
			
			System.out.println ("Desea continuar? [y,N]");
			opcion = scanner.next().charAt(0);
			
		} while ((opcion == 'Y') || (opcion == 'y'));
	}
}
