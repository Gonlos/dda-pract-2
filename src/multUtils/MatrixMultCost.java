package multUtils;

public class MatrixMultCost {
	private int cost;
	private IMatrix matrix;
	
	public MatrixMultCost(int cost, IMatrix op) {
		this.cost = cost;
		this.matrix = op;
	}
	
	public int getCost() {
		return cost;
	}
	
	public void setCost(int cost) {
		this.cost = cost;
	}
	
	public IMatrix getMatrix() {
		return matrix;
	}
	
	public void setOp(IMatrix matrix) {
		this.matrix = matrix;
	}
}
