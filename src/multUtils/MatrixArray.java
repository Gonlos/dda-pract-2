package multUtils;

public class MatrixArray implements IMatrix {
	private double[][] value;
	
	public MatrixArray(double[][] value) {
		// TODO Auto-generated constructor stub
		this.value = value;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return this.value[0].length;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return this.value.length;
	}
	
	@Override
	public String toString() {
		return "M(" + this.getHeight() + "," + this.getWidth() + ")";
	}

	@Override
	public double[][] getValue() {
		// TODO Auto-generated method stub
		return this.value;
	}
}