package multUtils;

import java.util.ArrayList;

public class MultAsocMatrices {
	private ArrayList<IMatrix> matrixList;
	private IMatrixFactory matrixFactory;
	
	public MultAsocMatrices( IMatrixFactory matrixFactory ) {
		this.matrixList = new ArrayList<IMatrix>();
		this.matrixFactory = matrixFactory;
	}
	
	public void addMatrix(IMatrix matrix) throws Exception {
		if ( this.matrixList.size() > 0 ) {
			if ( this.matrixList.get( this.matrixList.size() - 1
					).getWidth() != matrix.getHeight() ) {
				throw new Exception( "Las matriz añadida no se puede multiplicar con la anterior" );
			}
		}
		
		this.matrixList.add( matrix );
	}
	
	// Versión 2
	public MatrixMultCost getMultOrder() {
		int nMatrix = this.matrixList.size();
		int nComb = ( nMatrix  * (nMatrix + 1) ) / 2;
		MatrixMultCost[] partialMultiplication = new MatrixMultCost[nComb];
		
		int bStept = 0;
		int aIndex = 0;
		int bIndex = 0;
		MatrixMultCost multAct;
		MatrixMultCost multMin;
		
		int multAhBw = 0;
		
		int currentIndex = 0; 
		int currentStept = 0;
		
		currentIndex = 0;
		currentStept = nMatrix;
		// inicializamos el array para las matrices
		for( IMatrix m : this.matrixList) {
			partialMultiplication[ currentIndex ] = new MatrixMultCost(0,  m);
			currentIndex += currentStept--;
		}
		
		for( int startCol = 1; startCol < nMatrix; startCol++ ) {
			currentIndex = startCol;
			currentStept = nMatrix;
			for( int row = nMatrix-startCol; row > 0; row-- ) {
				aIndex = currentIndex - startCol;
				
				bStept = startCol + row - 1;
				bIndex = currentIndex + bStept;
				
				// dado que necesitamos siempre la multiplicación la precalculamos
				multAhBw = partialMultiplication[ aIndex ].getMatrix().getHeight() *
						   partialMultiplication[ bIndex ].getMatrix().getWidth();
				
				// ponemos un valor excesivamente grande como valor de partida (forzar el fallo)
				multMin = new MatrixMultCost(Integer.MAX_VALUE, null);
				
				// calculamos la nueva posicion de los indices
				// ya que queremos hacer las siguiente transicion
				// [A] X  X [I]        X [A] X [I]         X  X [A][I]          X  X  X[A,I] <- cuando ambos tienen el mismo
				//     X  X [B]   -->     X  X  X    -->      X  X  X    -->       X  X  X      índice hemos terminado
				//        X  X               X [B]               X  X                 X  X
				//           X                  X                  [B]                   X [B] <- fuera de índice
				//
				// Siendo:
				//   A: Conjunto de matrices al que llamaremos A
				//   B: Conjunto de matrices al que llamaremos B
				//   I: Conjunto de matrices del que queremos calcular su coste mínimo
				
				// mientras que no lleguemos a la casilla del elemento actual
				while( aIndex != currentIndex ) {
					// calculamos el coste como el coste de multiplicar A por B,
					// sumandole ademas el coste de obtener A y B
					multAct = new MatrixMultCost( partialMultiplication[ aIndex ].getCost() + 
									   		      partialMultiplication[ bIndex ].getCost() +
											      multAhBw * partialMultiplication[ aIndex ].getMatrix().getWidth(), 
											      this.matrixFactory.createMatrixMultiplication(
											    		  partialMultiplication[ aIndex ].getMatrix(),
											    		  partialMultiplication[ bIndex ].getMatrix() ) );
					
					// si el coste es inferior las cambiamos
					if ( multAct.getCost() < multMin.getCost() ) multMin = multAct;
					
					aIndex++;
					bIndex += --bStept;
				}
				
				partialMultiplication[ currentIndex ] = multMin;
				
				currentIndex += currentStept--;
			}
		}
		
		// devolvemos el resultado con coste mínimo
		return partialMultiplication[nMatrix-1];
	}

}
