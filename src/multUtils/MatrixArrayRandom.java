package multUtils;

import java.util.Random;

public class MatrixArrayRandom implements IMatrix {
	private MatrixArray matrixArray;
	
	public MatrixArrayRandom(int height, int width) {
		// TODO Auto-generated constructor stub
		Random r = new Random();
		double[][] matrixValue = new double[ height ][ width ];
		
		for( int i = height-1; i>=0; i-- )
			for( int j = width-1; j>=0; j-- )
				matrixValue[i][j] = r.nextDouble();
		
		this.matrixArray = new MatrixArray( matrixValue );
	}

	public int getWidth() {
		return matrixArray.getWidth();
	}

	public int getHeight() {
		return matrixArray.getHeight();
	}

	public String toString() {
		return matrixArray.toString();
	}

	public double[][] getValue() {
		return matrixArray.getValue();
	}

}