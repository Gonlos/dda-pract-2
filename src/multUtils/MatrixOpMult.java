package multUtils;

public class MatrixOpMult implements IMatrix {
	private IMatrix op1;
	private IMatrix op2;
	private int height;
	private int width;
	private double[][] value;
	
	public MatrixOpMult(IMatrix op1, IMatrix op2) {
		// TODO Auto-generated constructor stub
		this.op1 = op1;
		this.op2 = op2;
		this.height = op1.getHeight();
		this.width = op2.getWidth();
		this.value = null;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return width;
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return height;
	}
	
	@Override
	public String toString() {
		return "( " + op1.toString() + " * " + op2.toString() + " )";
	}
	
	private double[][] multiplicar( double[][] op1, double[][] op2) {
		int matrixOrder = op1[0].length;
		int height = op1.length;
		int width = op2[0].length;
		double[][] resultado = new double[height][width];
		
		for( int i = height-1; i >= 0; i-- ) {
			for( int j = width-1; j >= 0; j-- )
				resultado[i][j] = 0.0;
			
			for( int k = matrixOrder-1; k >= 0; k-- ) {
				for( int j = width-1; j >= 0; j-- ) {
					resultado[i][j] += op1[i][k] * op2[k][j];					
				}
			}
		}
		
		return resultado;
	}

	@Override
	public double[][] getValue() {
		// TODO Auto-generated method stub
		if ( this.value == null )
			this.value = this.multiplicar( this.op1.getValue(), this.op2.getValue() ); 
		
		return this.value;
	}
}