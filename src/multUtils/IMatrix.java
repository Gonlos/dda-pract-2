package multUtils;

public interface IMatrix {
	public int getWidth();
	public int getHeight();
	public double[][] getValue();
}
