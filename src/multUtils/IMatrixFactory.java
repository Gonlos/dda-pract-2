package multUtils;

public interface IMatrixFactory {
	public IMatrix createMatrixMultiplication( IMatrix op1, IMatrix op2 );
}
